#!/usr/bin/env python3
# Written by Shawn A 

def sing(number):
    """ Function takes a number and uses that to display out song to terminal
    Args:
        number (int): Integer with how many bottles go on the wall 
    Returns:
        None
    """
     
    for i in range(number, 0, -1):
        if i == 1:
            word = "1 bottle "
        else: 
            word = str(i) + " bottles "
        
        if i != number: 
            print(word + "of beer on the wall!\n") # This goes with set above; prints new line

        print(word + "of beer on the wall!")
        print(word + "of beer!")
        print("Take one down")
        print("And pass it around")

    print("No more bottles of beer on the wall!")

def main(): 
    import sys 

    userBottles =''
    if len(sys.argv) > 1:
        try: 
            userBottles = int(sys.argv[1])
            if userBottles < 100:
                sing(userBottles)
            else: 
                print("Sorry, max bottles is 99")
        except ValueError:
            print("Sorry, you did not enter an integer")    
    else: 
        sing(99)
        
    
if __name__ == "__main__":
    main()
